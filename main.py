import tkinter as tk
from tkinter import ttk
from nim import NimGameGUI
from marienbad import MarienbadGUI

class GameMenu:
    def __init__(self, root):
        self.root = root
        self.root.title("Game Selection Menu")
        self.root.geometry("300x300")

        self.main_frame = tk.Frame(self.root)
        self.main_frame.pack(expand=True, fill="both")
        self.main_frame.place(relx=0.5, rely=0.4, anchor="center")

        self.label = tk.Label(self.main_frame, text="Select a game to play", font=("Helvetica", 16))
        self.label.pack(pady=30)

        button_width = 15

        self.nim_button = ttk.Button(self.main_frame, text="Play Nim", width=button_width, command=self.select_nim_mode)
        self.nim_button.pack(pady=5)

        self.marienbad_button = ttk.Button(self.main_frame, text="Play Marienbad", width=button_width,
                                           command=self.select_marienbad_mode)
        self.marienbad_button.pack(pady=5)

    def select_nim_mode(self):
        self.show_mode_selection("Nim")

    def select_marienbad_mode(self):
        self.show_mode_selection("Marienbad")

    def show_mode_selection(self, game):
        self.clear_menu()

        self.label = tk.Label(self.root, text="Select mode of play", font=("Helvetica", 16))
        self.label.pack(pady=10)

        self.player1_label = tk.Label(self.root, text="Player 1:", font=("Helvetica", 12))
        self.player1_label.pack(pady=5)

        self.player1_type = ttk.Combobox(self.root, values=["Human", "AI Alpha-Beta", "Winning Strategy", "Attracteur"], state="readonly")
        self.player1_type.current(0)
        self.player1_type.pack(pady=5)

        self.player2_label = tk.Label(self.root, text="Player 2:", font=("Helvetica", 12))
        self.player2_label.pack(pady=5)

        self.player2_type = ttk.Combobox(self.root, values=["Human", "AI Alpha-Beta", "Winning Strategy", "Attracteur"], state="readonly")
        self.player2_type.current(0)
        self.player2_type.pack(pady=5)

        self.start_button = ttk.Button(self.root, text="Start Game", command=lambda: self.start_game(game))
        self.start_button.pack(pady=10)

        self.back_button = ttk.Button(self.root, text="Back", command=self.go_back_to_main)
        self.back_button.pack(pady=5)

    def clear_menu(self):
        for widget in self.root.winfo_children():
            widget.destroy()

    def go_back_to_main(self):
        self.clear_menu()
        self.__init__(self.root)

    def start_game(self, game):
        self.root.withdraw()
        player1_type = self.get_player_type(self.player1_type.get())
        player2_type = self.get_player_type(self.player2_type.get())

        if game == "Nim":
            game_root = tk.Toplevel(self.root)
            NimGameGUI(game_root, mode="Custom", players=[player1_type, player2_type])
            game_root.protocol("WM_DELETE_WINDOW", lambda: self.on_closing(game_root))
        elif game == "Marienbad":
            game_root = tk.Toplevel(self.root)
            MarienbadGUI(game_root, mode="Custom", players=[player1_type, player2_type])
            game_root.protocol("WM_DELETE_WINDOW", lambda: self.on_closing(game_root))

    def get_player_type(self, type_str):
        if type_str == "Human":
            return 0
        elif type_str == "AI Alpha-Beta":
            return 1
        elif type_str == "Winning Strategy":
            return -1
        elif type_str == "Attracteur":
            return -2

    def on_closing(self, window):
        window.destroy()
        self.root.deiconify()

if __name__ == "__main__":
    root = tk.Tk()
    app = GameMenu(root)
    root.mainloop()
