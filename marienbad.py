import random
import tkinter as tk
from threading import Thread
from tkinter import ttk
from tkinter import messagebox
from queue import Queue
from copy import deepcopy
from random import shuffle
from PIL import Image, ImageTk

inf = 10e6


def alpha_beta_decision(matches, turn, ai_level, queue, max_player):
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)
    best_move = possible_moves[0]
    best_value = -inf
    alpha = -inf
    beta = inf

    for move in possible_moves:

        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])
        value = min_value_ab(updated_matches, max_player, alpha, beta, ai_level, ai_level - 1)

        if value >= best_value:
            best_value = value
            best_move = move

    print("")
    queue.put(best_move)


def max_value_ab(matches, player, alpha, beta, ai_level, depth):
    if matches.check_victory():
        return 1000
    elif depth == 0:
        return 0

    value = -inf
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)

    for move in possible_moves:

        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])
        value = max(value, min_value_ab(updated_matches, player, alpha, beta, ai_level, depth - 1))

        if value >= beta:
            return value

        alpha = max(alpha, value)

    return value


def min_value_ab(matches, player, alpha, beta, ai_level, depth):
    if matches.check_victory():
        return -1000
    elif depth == 0:
        return 0

    value = inf
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)

    for move in possible_moves:

        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])
        value = min(value, max_value_ab(updated_matches, player, alpha, beta, ai_level, depth - 1))

        if value <= alpha:
            return value

        beta = min(beta, value)

    return value


def winning_strat(matches, queue):
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)
    best_move = possible_moves[0]

    for move in possible_moves:
        matches_binary = []
        res = []

        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])

        if updated_matches.check_victory_bis():
            best_move = move
            break

        if not (updated_matches.matches.count([1]) == 3 and updated_matches.matches.count(
                []) == 0) and not updated_matches.matches.count([1]) == 4:
            max_length = max(len(line) for line in updated_matches.matches)
            for i, line in enumerate(updated_matches.matches):
                matches_binary.append([int(i) for i in bin(len(line))[2:]])
                while len(matches_binary[i]) < max_length:
                    matches_binary[i].insert(0, 0)
            for i in range(max_length):
                xor_input = matches_binary[0][i]
                for j in range(1, len(matches_binary)):
                    xor_input = xor_input ^ matches_binary[j][i]

                res.append(xor_input)
            if sum(len(match_line) for match_line in updated_matches.matches) > 3:
                if res == [0] * max_length:
                    print(res)
                    best_move = move
            else:
                if res == [0] * (max_length - 1) + [1]:
                    best_move = move

    queue.put(best_move)



def winning_strat2(matches, queue):
    """this winning strat is too hard to use with this variant of marienbad"""
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)
    best_move = possible_moves[0]

    for move in possible_moves:
        res = 0

        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])

        if updated_matches.check_victory_bis():
            best_move = move
            break

        for line in updated_matches.matches:
            res += int(bin(len(line))[2:])

        if not (updated_matches.matches.count([1]) == 3 or updated_matches.matches.count(
                []) == 0) and not updated_matches.matches.count([1]) == 4:

            if sum(len(match_line) for match_line in updated_matches.matches) <= 3:
                if res % 2 == 1:
                    best_move = move
            else:
                if res % 2 == 0:
                    best_move = move

    queue.put(best_move)


def load_match_images():
    normal_image = Image.open("images/allumette_normale.png").convert("RGBA")
    normal_image = normal_image.resize((20, 60), Image.Resampling.LANCZOS)
    normal_photo = ImageTk.PhotoImage(normal_image)

    selected_image = Image.open("images/allumette_brulee.png").convert("RGBA")
    selected_image = selected_image.resize((20, 60), Image.Resampling.LANCZOS)
    selected_photo = ImageTk.PhotoImage(selected_image)

    return normal_photo, selected_photo


def winning_strategy3(matches, queue, player):
    best_move = []
    strat = matches.Strategie(player, (matches_to_tuple(matches.matches), player))
    if strat is None:
        best_move = matches.get_possible_moves()[0]
    else:
        sommet = tuple_to_matches(strat[0])
        for i in range(len(matches.matches)):
            res = len(matches.matches[i]) - len(sommet[i])
            if res > 0:
                best_move = [res, i]
        print(best_move)
    queue.put(best_move)


def matches_to_tuple(N):
    return tuple([tuple(n) for n in N])


def tuple_to_matches(N):
    return [list(n) for n in N]


class Matches:
    def __init__(self, matches=None):
        if matches is None:
            matches = sorted([[1 for _ in range(random.randint(1, 7))] for _ in range(random.randint(3, 5))], key=len)
        self.matches = matches
        self.Opp = {"Va": "Vb", "Vb": "Va"}  # dictionnaire des opposantes
        self.G = {}  # Dictionnaire du graphe: les clés sont les sommets, G[s]=la liste des succeeurs de s

    def reinit(self, matches=None):
        if matches is None:
            matches = sorted([[1 for _ in range(random.randint(1, 7))] for _ in range(random.randint(3, 5))], key=len)
        self.matches = matches
        self.Opp = {"Va": "Vb", "Vb": "Va"}  # dictionnaire des opposantes
        self.G = {}  # Dictionnaire du graphe: les clés sont les sommets, G[s]=la liste des succeeurs de s

    def check_victory(self):
        total_matches = sum(len(match_line) for match_line in self.matches)
        return total_matches == 0

    def check_victory_bis(self):
        total_matches = sum(len(match_line) for match_line in self.matches)
        return total_matches == 1

    def remove_matches(self, number, line_number):
        for _ in range(number):
            if self.matches[line_number]:
                self.matches[line_number].pop()

    def get_possible_moves(self):
        possible_moves = []
        for i in range(len(self.matches)):
            line = self.matches[i]
            if line:
                for j in range(1, len(line) + 1):
                    if self.valid_move(j, i):
                        possible_moves.append([j, i])
        return possible_moves

    def valid_move(self, number, line_number):
        if 1 <= number <= len(self.matches[line_number]):
            return True
        else:
            return False

    def copy(self):
        return deepcopy(self)

    def print_matches(self):
        for i in range(len(self.matches)):
            print(f"{i + 1} ", end="")
            for _ in range(5 - (len(self.matches[i]) // 2)):
                print(" ", end="")
            for j in range(len(self.matches[i])):
                if self.matches[i][j] == 1:
                    print("I", end="")
            print("\r")
        print("\n")

    def GrapheNim(self, N, joueur):
        """Renvoie le graphe du jeu de Nim partant de N allumettes avec joueur commence"""
        if (matches_to_tuple(N), joueur) not in self.G:  # Si on n'a pas encore rajouté ce sommet
            new_matches = Matches(tuple_to_matches(N))
            possible_moves = new_matches.get_possible_moves()
            succ = []
            for move in possible_moves:
                updated_matches = new_matches.copy()
                updated_matches.remove_matches(move[0], move[1])
                succ.append((matches_to_tuple(updated_matches.matches), self.Opp[joueur]))
            self.G[(matches_to_tuple(N), joueur)] = succ
        for (n, o) in self.G[(matches_to_tuple(N), joueur)]:
            self.GrapheNim(n, o)  # on va visiter ce successeur

    def Existence(self, X, s):
        """Renvoie True s’il existe un successeur de s dans X"""
        for succ in self.G[s]:
            if succ in X:  # l'un des successeurs est dans X
                return True
        return False

    def PourTout(self, X, s):
        """Renvoie True si tous les successeurs de s sont dans X"""
        for succ in self.G[s]:
            if succ not in X:  # l'un des successeurs n'est pas dans X
                return False
        return True  # Tous les successeurs sont dans X

    def F(self, X, j):
        L = []
        for s in self.G:
            if s not in X and s[1] == j and self.Existence(X, s):  # s est contrôlé par j
                L.append(s)
            if s not in X and s[1] != j and self.PourTout(X, s):  # s est contrôlé par l'adversaire
                L.append(s)
        return L

    def Attracteur(self, joueur):
        X = [(matches_to_tuple([[1] if i == j else [] for i in range(len(self.matches))]), self.Opp[joueur]) for j in
             range(len(self.matches))]  # Liste des sommets gagnants pour joueur, X=A_0
        Y = self.F(X, joueur)
        while len(Y) > 0:  # tant qu'il y a des choses à rajouter
            X = X + Y  # On actualise X, à la nième itération X=An
            Y = self.F(X, joueur)
        return X

    def Strategie(self, joueur, sommet):
        n = 0
        X = [(matches_to_tuple([[1] if i == j else [] for i in range(len(self.matches))]), self.Opp[joueur]) for j in
             range(len(self.matches))]  # Liste des sommets gagnants pour joueur, X=A_0
        print(X)
        R = {k: n for k in X}  # dictionnaire des rangs, R[s] est le rang de s
        while sommet not in R and n < 10000:  # tant qu'il y a des choses à rajouter
            n = n + 1  # on actualise n
            Y = self.F(X, joueur)  # on regarde les nouveaux sommets obtenus
            for k in Y:
                R[k] = n  # on déclare leur rang
            X = X + Y  # X=f(X)=f(A_{n-1})=A_n
        for v in self.G[sommet]:  # pour tous les successeurs de sommet
            if v in R and R[v] < R[sommet]:
                return v  # on retourne un successeur de sommet dans l'attracteur et de rang plus petit


class MarienbadGUI:
    def __init__(self, root, mode="Custom", players=[0, 0]):
        self.root = root
        self.root.title("Marienbad Game")
        self.root.geometry("500x400")
        self.matches = Matches()
        self.current_player = 1
        self.players = players  # [0: Human, 1: AI Alpha-Beta, -1: Winning Strategy Sprague Grundy, -2: Winning Strategy attracteur]
        self.ai_move = Queue()
        self.turn = 0
        self.selected_line = None
        self.blinking = False

        self.normal_photo, self.selected_photo = load_match_images()

        self.center_frame = tk.Frame(self.root)
        self.center_frame.pack (expand=True, fill=tk.BOTH)

        self.start_game()

    def start_game(self):
        self.matches.reinit()
        if self.players[0] == -2 or self.players[1] == -2:
            self.matches.GrapheNim(self.matches.matches, "Va")
        self.current_player = 1
        self.selected_line = None
        self.blinking = False

        self.matches_frame = tk.Frame(self.center_frame)
        self.matches_frame.pack()

        self.match_labels = []
        for i, match_line in enumerate(self.matches.matches):
            label_row = []
            for j, _ in enumerate(match_line):
                label = tk.Label(self.matches_frame, image=self.normal_photo)
                label.bind("<Button-1>", lambda event, index=(i, j): self.select_match(event, index))
                label.grid(row=i, column=j)
                label_row.append(label)
            self.match_labels.append(label_row)

        self.remove_button = ttk.Button(self.center_frame, text="Remove Matches", command=self.remove_matches)
        self.remove_button.pack(pady=10)

        self.player_label = tk.Label(self.center_frame, text=f"Player {self.current_player}'s turn",
                                     font=("Helvetica", 16))
        self.player_label.pack()

        self.handle_turn()

    def select_match(self, event, index):
        line_number, match_index = index
        if self.selected_line is not None and line_number != self.selected_line:
            return

        if any(label.cget("image") == str(self.normal_photo) for label in
               self.match_labels[line_number][match_index + 1:]):
            messagebox.showwarning("Warning", "Please select matches from right to left.")
            return

        label = self.match_labels[line_number][match_index]
        if label.cget("image") == str(self.normal_photo):
            label.config(image=self.selected_photo)
            self.selected_line = line_number
        else:
            label.config(image=self.normal_photo)

            if not any(label.cget("image") == str(self.selected_photo) for label in self.match_labels[line_number]):
                self.selected_line = None

    def remove_matches(self):
        if self.selected_line is None:
            messagebox.showwarning("Warning", "Please select at least one match to remove.")
            return

        selected_matches = sum(
            1 for label in self.match_labels[self.selected_line] if label.cget("image") == str(self.selected_photo))
        if selected_matches == 0:
            messagebox.showwarning("Warning", "Please select at least one match to remove.")
            return

        self.matches.remove_matches(selected_matches, self.selected_line)
        self.update_matches_display(self.selected_line)
        self.selected_line = None

        self.turn += 1
        self.current_player = 3 - self.current_player
        self.player_label.config(text=f"Player {self.current_player}'s turn")

        self.matches.print_matches()

        self.handle_turn()

    def update_matches_display(self, line_number):
        for label in self.match_labels[line_number]:
            label.config(image="")

        for i, match in enumerate(self.matches.matches[line_number]):
            if match == 1:
                self.match_labels[line_number][i].config(image=self.normal_photo)

    def handle_turn(self):
        if self.matches.check_victory():
            messagebox.showinfo("Game Over", f"Player {self.current_player} wins!")
            self.root.quit()
            return

        if self.players[self.current_player - 1] != 0:
            ai_thread = Thread(target=self.ai_turn)
            ai_thread.start()

    def ai_turn(self):
        if self.players[self.current_player - 1] == 1:
            alpha_beta_decision(self.matches, self.turn, 4, self.ai_move, self.current_player)
        elif self.players[self.current_player - 1] == -1:
            winning_strat(self.matches, self.ai_move)
        elif self.players[self.current_player - 1] == -2:
            if self.current_player - 1 == 0:
                winning_strategy3(self.matches, self.ai_move, "Va")
            else:
                winning_strategy3(self.matches, self.ai_move, "Vb")
        self.ai_wait_for_move()
        self.matches.print_matches()

    def move(self, movement):
        self.blinking = False
        self.matches.remove_matches(movement[0], movement[1])
        self.update_matches_display(movement[1])
        self.turn += 1
        self.current_player = 3 - self.current_player
        self.player_label.config(text=f"Player {self.current_player}'s turn")
        self.handle_turn()

    def ai_wait_for_move(self):
        if not self.ai_move.empty():
            movement = self.ai_move.get()
            self.blinking = True
            self.blink_matches(movement, 6)
        else:
            self.root.after(100, self.ai_wait_for_move)

    def blink_matches(self, movement, cycles):
        line_number, nb_matches = movement[1], movement[0]
        if cycles > 0 and self.blinking:
            for i in range(nb_matches):
                label = self.match_labels[line_number][i]
                current_image = label.cget("image")
                label.config(
                    image=self.selected_photo if current_image == str(self.normal_photo) else self.normal_photo)
            self.root.after(500, self.blink_matches, movement, cycles - 1)
        else:
            self.move(movement)


if __name__ == "__main__":
    root = tk.Tk()
    app = MarienbadGUI(root, mode="Custom", players=[1, -2])
    root.mainloop()
