import tkinter as tk
from tkinter import ttk, messagebox
from PIL import Image, ImageTk
from queue import Queue
from threading import Thread
from random import shuffle, randint

inf = 10e6


def alpha_beta_decision(matches, turn, ai_level, queue, max_player):
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)
    best_move = possible_moves[0]
    best_value = -inf
    alpha = -inf
    beta = inf

    for move in possible_moves:
        updated_matches = matches.copy()
        updated_matches.remove_matches(move)
        value = min_value_ab(updated_matches, max_player, alpha, beta, ai_level, ai_level - 1)

        if value >= best_value:
            best_value = value
            best_move = move

    queue.put(best_move)


def max_value_ab(matches, player, alpha, beta, ai_level, depth):
    if matches.check_victory():
        return 1000
    elif depth == 0:
        return 0

    value = -inf
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)

    for move in possible_moves:
        updated_matches = matches.copy()
        updated_matches.remove_matches(move)
        value = max(value, min_value_ab(updated_matches, player, alpha, beta, ai_level, depth - 1))

        if value >= beta:
            return value

        alpha = max(alpha, value)

    return value


def min_value_ab(matches, player, alpha, beta, ai_level, depth):
    if matches.check_victory():
        return -3000
    elif depth == 0:
        return 0

    value = inf
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)

    for move in possible_moves:
        updated_matches = matches.copy()
        updated_matches.remove_matches(move)
        value = min(value, max_value_ab(updated_matches, player, alpha, beta, ai_level, depth - 1))

        if value <= alpha:
            return value

        beta = min(beta, value)

    return value


def winning_strat(matches, queue):
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)
    best_move = possible_moves[0]

    for move in possible_moves:
        if (matches.matches - move) % 4 == 1:
            best_move = move

    queue.put(best_move)


def load_match_images():
    normal_image = Image.open("images/allumette_normale.png").convert("RGBA")
    normal_image = normal_image.resize((20, 60), Image.Resampling.LANCZOS)
    normal_photo = ImageTk.PhotoImage(normal_image)

    selected_image = Image.open("images/allumette_brulee.png").convert("RGBA")
    selected_image = selected_image.resize((20, 60), Image.Resampling.LANCZOS)
    selected_photo = ImageTk.PhotoImage(selected_image)

    return normal_photo, selected_photo

def winning_strategy3(matches, queue, player):
    strat = matches.Strategie(player, (matches.matches, player))
    if strat is None:
        best_move = matches.get_possible_moves()[0]
    else:
        best_move = matches.matches - strat[0]
    queue.put(best_move)


class Matches:
    def __init__(self):
        self.matches = randint(15, 30)
        self.Opp = {"Va": "Vb", "Vb": "Va"}  # dictionnaire des opposantes
        self.G = {}  # Dictionnaire du graphe: les clés sont les sommets, G[s]=la liste des succeeurs de s

    def reinit(self):
        self.matches = randint(15, 30)
        self.Opp = {"Va": "Vb", "Vb": "Va"}  # dictionnaire des opposantes
        self.G = {}  # Dictionnaire du graphe: les clés sont les sommets, G[s]=la liste des succeeurs de s

    def check_victory(self):
        return self.matches == 0

    def remove_matches(self, number):
        self.matches = max(0, self.matches - number)

    def get_possible_moves(self):
        return list(range(1, min(self.matches, 3) + 1))

    def copy(self):
        new_matches = Matches()
        new_matches.matches = self.matches
        return new_matches

    def GrapheNim(self, N, joueur):
        """Renvoie le graphe du jeu de Nim partant de N allumettes avec joueur commence"""
        if (N, joueur) not in self.G:  # Si on n'a pas encore rajouté ce sommet
            self.G[(N, joueur)] = [(k, self.Opp[joueur]) for k in range(max(N - 3, 1), N)]  # Liste des successeurs de (N,joueur)
        for (n, o) in self.G[(N, joueur)]:
            self.GrapheNim(n, o)  # on va visiter ce successeur

    def Existence(self, X, s):
        """Renvoie True s’il existe un successeur de s dans X"""
        for succ in self.G[s]:
            if succ in X:  # l'un des successeurs est dans X
                return True
        return False

    def PourTout(self, X, s):
        """Renvoie True si tous les successeurs de s sont dans X"""
        for succ in self.G[s]:
            if succ not in X:  # l'un des successeurs n'est pas dans X
                return False
        return True  # Tous les successeurs sont dans X

    def F(self, X, j):
        L = []
        for s in self.G:
            if s not in X and s[1] == j and self.Existence(X, s):  # s est contrôlé par j
                L.append(s)
            if s not in X and s[1] != j and self.PourTout(X, s):  # s est contrôlé par l'adversaire
                L.append(s)
        return L

    def Attracteur(self, joueur):
        X = [(1, self.Opp[joueur])]  # Liste des sommets gagnants pour joueur: X=A0
        Y = self.F(X, joueur)
        while len(Y) > 0:  # tant qu'il y a des choses à rajouter
            X = X + Y  # On actualise X, à la nième itération X=An
            Y = self.F(X, joueur)
        return X

    def Strategie(self, joueur, sommet):
        n = 0
        X = [(1, self.Opp[joueur])]  # Liste des sommets gagnants pour joueur, X=A_0
        R = {k: n for k in X}  # dictionnaire des rangs, R[s] est le rang de s
        while sommet not in R and n < 1000:  # tant qu'il y a des choses à rajouter
            n = n + 1  # on actualise n
            Y = self.F(X, joueur)  # on regarde les nouveaux sommets obtenus
            for k in Y:
                R[k] = n  # on déclare leur rang
            X = X + Y  # X=f(X)=f(A_{n-1})=A_n
        for v in self.G[sommet]:  # pour tous les successeurs de sommet
            if v in R and R[v] < R[sommet]:
                return v  # on retourne un successeur de sommet dans l'attracteur et de rang plus petit


class NimGameGUI:
    def __init__(self, root, mode, players):
        self.root = root
        self.root.title("Nim Game")
        self.root.geometry("550x150")
        self.matches = Matches()
        self.current_player = 1
        self.selected_matches = []
        self.human_turn = False
        self.ai_move = Queue()
        self.players = players
        self.mode = mode
        self.normal_match_image, self.selected_match_image = load_match_images()
        self.is_first_turn = True

        self.center_frame = tk.Frame(self.root)
        self.center_frame.pack(expand=True, fill=tk.BOTH)

        self.matches_frame = None
        self.match_labels = []

        self.start_game()

    def start_game(self):
        self.matches.reinit()
        self.matches.GrapheNim(self.matches.matches, "Va")
        window_width = self.matches.matches * 25
        self.root.geometry(f"{window_width + 20}x160")
        self.current_player = 1
        self.selected_matches.clear()
        self.is_first_turn = True

        self.matches_frame = tk.Frame(self.center_frame, width=window_width, height=80)
        self.matches_frame.pack()
        self.matches_frame.pack_propagate(False)

        self.match_labels = []
        for i in range(self.matches.matches):
            label = tk.Label(self.matches_frame, image=self.normal_match_image)
            label.bind("<Button-1>", lambda event, label_l=label, index=i: self.select_match(label_l, index))
            label.pack(side=tk.LEFT)
            self.match_labels.append(label)

        self.remove_button = ttk.Button(self.center_frame, text="Remove Matches", command=self.remove_matches)
        self.remove_button.pack(pady=10)

        self.player_label = tk.Label(self.center_frame, text=f"Player {self.current_player}'s turn",
                                     font=("Helvetica", 16))
        self.player_label.pack()

        self.handle_turn()

    def select_match(self, label, index):
        if index != self.matches.matches - 1 - len(self.selected_matches):
            return

        if len(self.selected_matches) < 3:
            if label.cget("image") == str(self.normal_match_image):
                label.config(image=self.selected_match_image)
                self.selected_matches.append(index)
            elif label.cget("image") == str(self.selected_match_image):
                label.config(image=self.normal_match_image)
                self.selected_matches.pop()

    def handle_turn(self):
        if self.matches.check_victory():
            winner = 1 if self.current_player == 1 else 2
            messagebox.showinfo("Game Over", f"Player {winner} wins!")
            self.root.destroy()
            return

        if self.players[self.current_player - 1] == 0:
            self.human_turn = True
        else:
            self.human_turn = False
            self.perform_ai_move()

    def perform_ai_move(self):
        if self.players[self.current_player - 1] == 1:
            ai_thread = Thread(target=alpha_beta_decision,
                               args=(self.matches, self.current_player, 5, self.ai_move, self.current_player))
        elif self.players[self.current_player - 1] == -1:
            ai_thread = Thread(target=winning_strat, args=(self.matches, self.ai_move))
        elif self.players[self.current_player - 1] == -2:
            if self.current_player - 1 == 0:
                ai_thread = Thread(target=winning_strategy3, args=(self.matches, self.ai_move, "Va"))
            else:
                ai_thread = Thread(target=winning_strategy3, args=(self.matches, self.ai_move, "Vb"))

        ai_thread.start()
        self.root.after(100, self.check_ai_move)

    def check_ai_move(self):
        if not self.ai_move.empty():
            move = self.ai_move.get()
            self.blink_matches(move)
        else:
            self.root.after(100, self.check_ai_move)

    def blink_matches(self, move):
        matches_to_blink = self.match_labels[-move:]
        self.blink_count = 0

        def blink():
            if self.blink_count < 6:
                for label in matches_to_blink:
                    current_image = label.cget("image")
                    label.config(image=self.selected_match_image if current_image == str(
                        self.normal_match_image) else self.normal_match_image)
                self.blink_count += 1
                self.root.after(500, blink)
            else:
                self.remove_matches_ai(move)

        blink()

    def remove_matches_ai(self, move):
        self.matches.remove_matches(move)
        self.update_matches_display(move)
        self.current_player = 2 if self.current_player == 1 else 1
        self.player_label.config(text=f"Player {self.current_player}'s turn")
        self.is_first_turn = False
        self.handle_turn()

    def remove_matches(self):
        if self.human_turn and 0 < len(self.selected_matches) <= 3:
            self.matches.remove_matches(len(self.selected_matches))
            self.update_matches_display(len(self.selected_matches))
            self.selected_matches.clear()
            self.current_player = 2 if self.current_player == 1 else 1
            self.player_label.config(text=f"Player {self.current_player}'s turn")
            self.is_first_turn = False
            self.handle_turn()

    def update_matches_display(self, removed_count):
        for _ in range(removed_count):
            label = self.match_labels.pop()
            label.pack_forget()

    def reset_game(self):
        self.matches_frame.pack_forget()
        self.remove_button.pack_forget()
        self.player_label.pack_forget()
        self.start_button.pack()


if __name__ == "__main__":
    root = tk.Tk()
    app = NimGameGUI(root, mode="Custom", players=[0, -2])
    root.mainloop()
