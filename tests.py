import random
from threading import Thread
from queue import Queue
from copy import deepcopy
from random import shuffle
from datetime import datetime

inf = 10e6

def alpha_beta_decision(matches, turn, ai_level, queue, max_player, complexity):
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)
    best_move = possible_moves[0]
    best_value = -inf
    alpha = -inf
    beta = inf

    for move in possible_moves:
        complexity[1] += 1
        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])
        value = min_value_ab(updated_matches, max_player, alpha, beta, ai_level, ai_level - 1, complexity)

        if value >= best_value:
            best_value = value
            best_move = move

    print("")
    queue.put(best_move)

def max_value_ab(matches, player, alpha, beta, ai_level, depth, complexity):
    if matches.check_victory():
        return 1000
    elif depth == 0:
        return 0

    value = -inf
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)

    for move in possible_moves:
        complexity[1] += 1
        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])
        value = max(value, min_value_ab(updated_matches, player, alpha, beta, ai_level, depth - 1, complexity))

        if value >= beta:
            return value

        alpha = max(alpha, value)

    return value

def min_value_ab(matches, player, alpha, beta, ai_level, depth, complexity):
    if matches.check_victory():
        return -1000
    elif depth == 0:
        return 0

    value = inf
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)

    for move in possible_moves:
        complexity[1] += 1
        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])
        value = min(value, max_value_ab(updated_matches, player, alpha, beta, ai_level, depth - 1, complexity))

        if value <= alpha:
            return value

        beta = min(beta, value)

    return value

def winning_strat(matches, queue, complexity):
    possible_moves = matches.get_possible_moves()
    shuffle(possible_moves)
    best_move = possible_moves[0]

    for move in possible_moves:
        complexity[-1] += 1
        matches_binary = []
        res = []

        updated_matches = matches.copy()
        updated_matches.remove_matches(move[0], move[1])

        if updated_matches.check_victory_bis():
            best_move = move
            break

        if not (updated_matches.matches.count([1]) == 3 and updated_matches.matches.count(
                []) == 0) and not updated_matches.matches.count([1]) == 4:
            max_length = max(len(line) for line in updated_matches.matches)
            for i, line in enumerate(updated_matches.matches):
                complexity[-1] += 1
                matches_binary.append([int(i) for i in bin(len(line))[2:]])
                while len(matches_binary[i]) < max_length:
                    matches_binary[i].insert(0, 0)
            for i in range(max_length):
                complexity[-1] += 1
                xor_input = matches_binary[0][i]
                for j in range(1, len(matches_binary)):
                    xor_input = xor_input ^ matches_binary[j][i]

                res.append(xor_input)
            if sum(len(match_line) for match_line in updated_matches.matches) > 3:
                if res == [0] * max_length:
                    best_move = move
            else:
                if res == [0] * (max_length - 1) + [1]:
                    best_move = move

    queue.put(best_move)

def winning_strategy3(matches, queue, player, complexity):
    best_move = []
    strat = matches.Strategie(player, (matches_to_tuple(matches.matches), player, complexity))
    if strat is None:
        best_move = matches.get_possible_moves()[0]
    else:
        sommet = tuple_to_matches(strat[0])
        for i in range(len(matches.matches)):
            res = len(matches.matches[i]) - len(sommet[i])
            if res > 0:
                best_move = [res, i]
        print(best_move)
    queue.put(best_move)

def matches_to_tuple(N):
    return tuple([tuple(n) for n in N])

def tuple_to_matches(N):
    return [list(n) for n in N]

class Matches:
    def __init__(self, matches=None):
        if matches is None:
            matches = sorted([[1 for _ in range(random.randint(1, 7))] for _ in range(random.randint(3, 5))], key=len)
        self.matches = matches
        self.Opp = {"Va": "Vb", "Vb": "Va"}
        self.G = {}

    def reinit(self, matches=None):
        if matches is None:
            matches = sorted([[1 for _ in range(random.randint(1, 7))] for _ in range(random.randint(3, 5))], key=len)
        self.matches = matches
        self.Opp = {"Va": "Vb", "Vb": "Va"}
        self.G = {}

    def check_victory(self):
        total_matches = sum(len(match_line) for match_line in self.matches)
        return total_matches == 0

    def check_victory_bis(self):
        total_matches = sum(len(match_line) for match_line in self.matches)
        return total_matches == 1

    def remove_matches(self, number, line_number):
        for _ in range(number):
            if self.matches[line_number]:
                self.matches[line_number].pop()

    def get_possible_moves(self):
        possible_moves = []
        for i in range(len(self.matches)):
            line = self.matches[i]
            if line:
                for j in range(1, len(line) + 1):
                    if self.valid_move(j, i):
                        possible_moves.append([j, i])
        return possible_moves

    def valid_move(self, number, line_number):
        if 1 <= number <= len(self.matches[line_number]):
            return True
        else:
            return False

    def copy(self):
        return deepcopy(self)

    def print_matches(self):
        for i in range(len(self.matches)):
            print(f"{i + 1} ", end="")
            for _ in range(5 - (len(self.matches[i]) // 2)):
                print(" ", end="")
            for j in range(len(self.matches[i])):
                if self.matches[i][j] == 1:
                    print("I", end="")
            print("\r")
        print("\n")

    def GrapheNim(self, N, joueur, complexity):
        if (matches_to_tuple(N), joueur) not in self.G:
            new_matches = Matches(tuple_to_matches(N))
            possible_moves = new_matches.get_possible_moves()
            succ = []
            for move in possible_moves:
                complexity[-2] += 1
                updated_matches = new_matches.copy()
                updated_matches.remove_matches(move[0], move[1])
                succ.append((matches_to_tuple(updated_matches.matches), self.Opp[joueur]))
            self.G[(matches_to_tuple(N), joueur)] = succ
        for (n, o) in self.G[(matches_to_tuple(N), joueur)]:
            self.GrapheNim(n, o)

    def Existence(self, X, s):
        for succ in self.G[s]:
            if succ in X:
                return True
        return False

    def PourTout(self, X, s):
        for succ in self.G[s]:
            if succ not in X:
                return False
        return True

    def F(self, X, j):
        L = []
        for s in self.G:
            if s not in X and s[1] == j and self.Existence(X, s):
                L.append(s)
            if s not in X and s[1] != j and self.PourTout(X, s):
                L.append(s)
        return L

    def Attracteur(self, joueur):
        X = [(matches_to_tuple([[1] if i == j else [] for i in range(len(self.matches))]), self.Opp[joueur]) for j in
             range(len(self.matches))]
        Y = self.F(X, joueur)
        while len(Y) > 0:
            X = X + Y
            Y = self.F(X, joueur)
        return X

    def Strategie(self, joueur, sommet, complexity):
        n = 0
        X = [(matches_to_tuple([[1] if i == j else [] for i in range(len(self.matches))]), self.Opp[joueur]) for j in
             range(len(self.matches))]
        R = {k: n for k in X}
        while sommet not in R and n < 10000:
            complexity[-2] += 1
            n = n + 1
            Y = self.F(X, joueur)
            for k in Y:
                R[k] = n
            X = X + Y
        for v in self.G[sommet]:
            if v in R and R[v] < R[sommet]:
                return v

class MarienbadConsole:
    def __init__(self, mode="Custom", players=[0, 0], results=None, time_algo=None, complexity=None):
        self.matches = Matches()
        self.current_player = 1
        self.players = players
        self.ai_move = Queue()
        self.turn = 0
        if mode == "Random":
            self.matches.reinit()
        elif mode == "Custom":
            self.matches.reinit(self.matches.matches)
        elif mode == "Attracteur":
            if self.players[0] == -2 or self.players[1] == -2:
                self.matches.GrapheNim(self.matches.matches, "Va", complexity)
        self.current_player = 1
        self.handle_turn(results, time_algo, complexity)

    def handle_turn(self, results, time_algo, complexity):
        while not self.matches.check_victory():
            print(f"Player {self.current_player}'s turn")
            self.matches.print_matches()

            if self.players[self.current_player - 1] == 0:
                self.human_turn()
            else:
                start_time = datetime.now()
                self.ai_turn(complexity)

            if time_algo is not None:
                delta = datetime.now() - start_time
                time_algo[self.players[self.current_player - 1]] += int(delta.total_seconds() * 1000)

            self.turn += 1
            self.current_player = 3 - self.current_player

        print(f"Player {self.current_player} wins!")
        if results is not None:
            results[self.players[self.current_player - 1]] += 1



    def human_turn(self):
        line_number = int(input("Enter the line number: ")) - 1
        num_matches = int(input("Enter the number of matches to remove: "))
        if self.matches.valid_move(num_matches, line_number):
            self.matches.remove_matches(num_matches, line_number)
        else:
            print("Invalid move. Try again.")
            self.human_turn()

    def ai_turn(self, complexity):
        if self.players[self.current_player - 1] == 1:
            alpha_beta_decision(self.matches, self.turn, 7, self.ai_move, self.current_player, complexity)
        elif self.players[self.current_player - 1] == -1:
            winning_strat(self.matches, self.ai_move, complexity)
        elif self.players[self.current_player - 1] == -2:
            if self.current_player - 1 == 0:
                winning_strategy3(self.matches, self.ai_move, "Va", complexity)
            else:
                winning_strategy3(self.matches, self.ai_move, "Vb", complexity)

        move = self.ai_move.get()
        self.matches.remove_matches(move[0], move[1])
        print(f"AI Player {self.current_player} removed {move[0]} matches from line {move[1] + 1}")

if __name__ == "__main__":
    results = {-1: 0, 1: 0}  # Dictionary to store the results
    time_algo = {-1: 0, 1: 0}    # Dictionary to store the time
    complexity = {-1: 0, 1: 0}  # Dictionary to store complexity
    players_types = [
        (1, -1), (-1, 1)
    ]

    repetitions = 10

    for i in range(int(repetitions/2)):
        print("Partie", i)
        players = [1, -1]
        app = MarienbadConsole(players=players, results=results, time_algo=time_algo, complexity=complexity)

    for i in range(int(repetitions/2), repetitions):
        print("Partie", i)
        players = [-1, 1]
        app = MarienbadConsole(players=players, results=results, time_algo=time_algo, complexity=complexity)


    print("Results after 1000 games:")
    print(f"AI Alpha-Beta wins: {results[1]}")
    print(f"Winning Strategy for Sprague-Grundy wins: {results[-1]}")
    print(f"Mean time for Alpha-beta : {time_algo[1]/repetitions}")
    print(f"Mean time for Sprague-Grundy : {time_algo[-1] / repetitions}")
    print(f"Complexity for Alpha-beta: {complexity[1]/repetitions}")
    print(f"Complexity for Sprague-grundy: {complexity[-1]/repetitions}")
